import laudion
import unittest

class TestSession(unittest.TestCase):
    def test_close():
        sess = laudion.Session()
        sess.close()
        self.assertRaises(RuntimeError, sess.run)