from distutils.core import setup
from Cython.Distutils import build_ext
from distutils.extension import Extension

setup(
  name = 'laudion',
  version = '1.0.0',
  license='MIT',
  ext_modules = cythonize(["laudion/Session.pyx"]),
  description = 'faster neural network hardware',
  author = 'TheNeuronalCoder',
  url = 'https://github.com/laudion/laudion',
#   download_url = 'https://gitlab.com/laudion/laudion/archive/v1.tar.gz',
  classifiers=[
    'Development Status :: 3 - Alpha',      # Chose either "3 - Alpha", "4 - Beta" or "5 - Production/Stable" as the current state of your package
    'Intended Audience :: Developers',
    'License :: OSI Approved :: MIT License',
    'Programming Language :: Python :: 3',
    'Programming Language :: Python :: 3.4',
    'Programming Language :: Python :: 3.5',
    'Programming Language :: Python :: 3.6',
  ]
)