class Session {
    private:
        bool session_open;
    public:
        Session() {
            session_open = true;
        }

        void close();
};

extern "C" {
    Session* new_session() { return new Session(); }
    void close_session(Session* sess) { sess->close(); }
} 