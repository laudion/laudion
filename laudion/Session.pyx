from libc.Session import Session

cdef class Session(object):
    cdef private bool open
    cpdef __init__(self, string target='', graph=None, config=None):
        self.open = True

        self.graph = graph
        if self.graph == None:
            self.graph_def = None
        else:
            self.graph_def = self.graph.as_graph_def()

    cpdef run(self, fetches, feed_dict=None, options=None, run_metadata=None):
        if self.open:
            pass
        else:
            raise RuntimeError

    cpdef close(self):
        self.open = False